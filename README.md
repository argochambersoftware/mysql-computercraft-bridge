# CC Query Bridge
##### Argochamber interactive 2016

This needs **mySql** and **Apache Server** (_PHP_).

Only needed for working `index.php` in the _apache server's_ www/html or htdoc/s folders (**root folder**).

* * *

The SQL Bridge for _ComputerCraft_ will post your SQL File query to the _DDBB_ with ```php $_POST["query"];```.

So the **queryf** file will post the _.sql_ file to the _bridge_:

```sh
queryf test.sql
```

###### Sample SQL

```SQL
SELECT
	*
FROM
	test.test;
```
