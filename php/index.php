<?php


$db = new mysqli("127.0.0.1", "root", "1234", "computercraft");

if ($db->connect_errno) {
    echo "SQL Connection Failure (" . $db->connect_errno . ") " . $db->connect_error;
}

$query = $db->query($_POST["query"]);

$rows = array();
while($r = mysqli_fetch_assoc($query)) {
    $rows[] = $r;
}
print json_encode($rows);
