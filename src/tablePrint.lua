--[[
	Argochamber Interactive 2016
	Prints a table like a table.
]]
local function printTable(tab, level)

	level = level or 1;
	
	local s = "";	
	for i = 1, level do
		s = s.." ";
	end

	for k, v in pairs(tab) do

		if (type(v) == "table") then
			print(s.."["..k.."] {");
			printTable(v, level+1);
			print(s.."}");
		else
			local str = "";
			if type(v) == "string" then
				for i = 1, v:len() do
					local s = v:sub(i,i);
					local b = s:byte();
					if b <= 32 or b >= 127 then
						str = str.." ";
					else
						str = str..s;
					end
				end
			else
				str = v;
			end
			print(s..k.." -> "..str);
		end

	end

end

return printTable
