--[[
	Table printing API
	Print Nicely tables!
	---
	Argochamber Interactive 2016
]]

Tables = {};

local DOT_THRESHOLD = 2;

function truncateStr(str, len)
	local s = "";

	for i = 1, len do
		if i > (len-DOT_THRESHOLD) and str:len() > (len-DOT_THRESHOLD) then
			s = s..".";
		elseif str:len() < i then
			s = s.." ";
		else
			s = s..str:sub(i,i);
		end
	end
	
	return s;
end

local ODD_COLOR = '7';
local EVEN_COLOR = '8';
local ODD_HEADER = 'd';
local EVEN_HEADER = '5';

--[[
	This will not print recursively! only the first layer! (An SQL Like tables)
	BUT: The table doesn't know about the schema, be ware!
		So remember to include at least one row full of nulls or the table will display a single null.
]]
Tables.draw = function(tbl, mon)

	mon = mon or term;
	
	-- First we must calculate the <- Width -> of the table.
	local w, rows = mon.getSize();
	rows = rows - 1; -- Calculates the rows.
	
	local schema = 0;
	
	local schemaKeys = {};
	for k, v in pairs(tbl[1]) do
		table.insert(schemaKeys, k);
		schema = schema + 1;
	end

	local wPerCol = math.ceil( w/(schema) ) - 1; -- Minus one because the index will be shown.
	if w-wPerCol < 2 then
		wPerCol = math.floor( w/(schema) ) - 1;
	end
	
	-- Start the render job.
	mon.clear();
	
	local fgBar = "";
	local blackbar = "";
	for i = 1, w do
		fgBar = fgBar..'0';
		blackbar = blackbar..'f';
	end

	-- Build the headers.
	mon.setCursorPos(1, 1);
	
	local header = "n"..string.rep(" ", (w-(schema*wPerCol))-1);
	local headerBg = string.rep(EVEN_HEADER, w-(schema*wPerCol));
	local resultBg = string.rep(EVEN_COLOR, w-(schema*wPerCol));
	local odd = true;
	for i = 1, schema do
		header = header..truncateStr(schemaKeys[i], wPerCol);
		for j = 1, wPerCol do
			if odd == true then
				headerBg = headerBg..ODD_HEADER;
				resultBg = resultBg..ODD_COLOR;
			else
				headerBg = headerBg..EVEN_HEADER;
				resultBg = resultBg..EVEN_COLOR;
			end
		end
		odd = not odd;
	end
	
	mon.blit(header, blackbar, headerBg);

	-- ## FINALLY RENDER THE RESULTS OF THE QUERY ## --
	local dumpLast = false;
	if #tbl > rows then
		dumpLast = true;
	end
	
	for i = 0, rows do
		local iNum = tostring(i);
		local resultBuilt = iNum..string.rep(" ", (w-(schema*wPerCol))-iNum:len());
		local tval = tbl[i] or false;
		if tval == false then
			for j = 1, schema do
				resultBuilt = resultBuilt..truncateStr("", wPerCol);
			end
		else
			for k, v in pairs(tval) do
				resultBuilt = resultBuilt..truncateStr(v, wPerCol);
			end
		end
		mon.blit(resultBuilt, fgBar, resultBg);
		mon.setCursorPos(1, i+2);
	end
	if dumpLast == true then
		local buildDump = "#  ";
		for j = 1, schema do
			buildDump = buildDump..truncateStr("...", wPerCol);
		end
		mon.blit(buildDump, fgBar, resultBg);
		mon.setCursorPos(1, rows+1);
	end
	
	mon.setCursorPos(1, rows+1);
	
end
